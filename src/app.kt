class Person(val name: String)

fun main(){
    val person = Person("Mariusz")

    println("Hello ${person.name} World")
}

